package exception;

/**
 * 常见面试题
 * 1:请分别说明final finally finalize
 *   final是一个关键字。可以修饰三个地方
 *   在类上:类不能被继承
 *   在方法上:该方法不能被重写
 *   在变量上:该值不可改变
 *
 *   finally是一个关键字，表示异常处理机制的最后一块。
 *   finally块的特点:程序执行到try语句块中，无论try中代码是否出现异常，最终都要执行finally块
 *                  通常我们将释放资源这类操作放在finally中确保执行，比如IO中的close()
 *
 *   finalize是Object中定义的一个方法，这意味着java中所有的类都有这个方法。
 *   该方法何时被调用:当一个对象不再被引用时，此时GC会将其回收来释放资源，在释放前会调用
 *                 该方法，这意味着该方法执行完毕后这个对象即被释放。
 *                 它是一个对象生命周期中最后一个被执行的方法，是GC执行。
 *                 java手册上对该方法有明确说明:该方法若被重写不应当有耗时的操作，否则会
 *                 直接影响GC的回收性能!!
 *
 * 2:下面代码
 *
 */
public class FinallyDemo3 {
    public static void main(String[] args) {
        System.out.println(
                test("a")+","+test(null)+","+test("")
        );
        //结果是:3,3,3
    }
    public static int test(String str){
        try {
            /*
                两个字符相减，会先换成int再相减，所以结果也是int
             */
            return str.charAt(0)-'a';//相同字符相减结果为0，字符相减实际上是字符对应的unicode编码换算的整数进行相减操作 比如:'a'-'a'等价于97-97
        }catch(NullPointerException e){
            return 1;
        }catch(Exception e){
            return 2;
        }finally{
            /*
                警告的原因是:finally中添加了return则意味着方法一定返回finally中的结果
             */
            return 3;//编译器会提示警告信息:表达在finally中添加了return语句
        }

    }
}
