package exception;

/**
 * 自定义异常
 * 常用于描述那些满足语法但是不满足业务所导致的问题。
 * 创建一个自定义异常需要做到以下几点:
 * 1:类名要见名知义
 * 2:需要是Exception的子类(直接或间接继承都可以)
 * 3:提供超类异常提供的所有构造器
 */
public class IllegalAgeException extends Exception{
    public IllegalAgeException() {
    }

    public IllegalAgeException(String message) {
        super(message);
    }

    public IllegalAgeException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalAgeException(Throwable cause) {
        super(cause);
    }

    public IllegalAgeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
