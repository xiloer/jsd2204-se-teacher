package exception;

/**
 * 异常的抛出
 *
 * throw关键字可以主动对外抛出一个异常实例。通常遇到下列情况是我们可以主动对外抛出异常
 * 1:程序运行时出现了异常，但是该异常不应当在当前代码片段被处理时，可以主动抛出给调用者
 * 2:程序可以运行，但是不满足业务场景时，可以当作异常抛出给调用者。(本案例就演示这种情况)
 */
public class Person {
    private int age;//年龄

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws IllegalAgeException {
        if(age<0||age>100){
            //如果程序执行到这里，就会将实例化的异常抛出到当前方法setAge方法之外
            //意味着的谁调用setAge方法，谁处理这个异常
            /*
                RuntimeException称为运行时异常。它也是编译器不检查处理手段的异常。
                这意味着抛出该异常时，编译器不强制要求我们写处理方法(try-catch或throws)。
                RuntimeException常见子类:
                NullPointerException,ArrayIndexOutOfBoundsException,ClassCastException
                NumberFormatException...
                RuntimeException的这里异常通常都是因为程序漏洞(bug)引发的异常，他们通常都是可以
                通过修复逻辑漏洞解决的。这类异常就不应当用异常处理机制来处理。

             */
//            throw new RuntimeException("年龄不合法!");
            /*
                通常throw什么异常就必须在当前方法上使用throws声明该异常的抛出来通知
                调用者当前方法可能会出现的异常，提示其添加处理该异常的手段
                只有RuntimeException是一个例外，抛出时不强制要求。
             */
//            throw new Exception("年龄不合法!");

            throw new IllegalAgeException("年龄超过了范围(0-100)");
        }
        this.age = age;
    }
}
