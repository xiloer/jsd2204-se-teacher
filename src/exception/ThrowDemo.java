package exception;

/**
 * 演示异常的抛出
 */
public class ThrowDemo {
    public static void main(String[] args){
        Person p = new Person();
//        p.setAge(20);
        /*
            异常的打桩信息:
                                                 什么异常(通过它得知出现的什么问题)
            Exception in thread "main" java.lang.RuntimeException: 年龄不合法!
            at exception.Person.setAge(Person.java:21)      第一行(第一个at)时该异常原发位置
            at exception.ThrowDemo.main(ThrowDemo.java:10)  第二行。出现第二行甚至其他行，说明上面一行出现异常的位置没有处理该异常，而是将这个异常抛出到当前位置
         */
        /*
            当我们调用一个含有throws声明异常抛出的方法时，编译器要求我们必须处理该异常
            处理方式有两种可选:
            1:使用try-catch捕获并处理该异常
            2:可以继续在当前方法上使用throws声明该异常的抛出继续通知上层调用者处理该异常
            具体使用那种取决于实际业务场景中异常处理的责任制问题。

            注意:实际开发中永远不要在main方法上写throws!
         */
        try {
            p.setAge(10000) ;//满足语法，但是不满足业务场景
        } catch (IllegalAgeException e) {
            e.printStackTrace();
        }

        System.out.println("此人年龄:"+p.getAge()+"岁");
    }
}
