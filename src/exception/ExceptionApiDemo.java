package exception;

/**
 * 异常类的常用方法
 */
public class ExceptionApiDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了");
        String str = "abc";
        try{
            System.out.println(Integer.parseInt(str));
        }catch(Exception e){
            //该方法就是Exception最常被调用的方法，作用是在控制台上输出异常的堆栈信息
            //便于我们跟踪错误出现的位置进行debug
            e.printStackTrace();

            //getMessage()方法获取错误消息通常用于记录日志或提示给用户
            String message = e.getMessage();
            System.out.println(message);

        }
        System.out.println("程序结束了");
    }
}
