package file;

import java.io.File;

/**
 * 删除一个文件
 */
public class DeleteFileDemo {
    public static void main(String[] args) {
        //将当前目录下的test.txt文件删除
        File file = new File("./test.txt");
        //如果该文件存在
        if(file.exists()){
            //delete:删除
            file.delete();//该方法用于将file表示的文件或目录删除。
            System.out.println("该文件已删除!");
        }else{
            System.out.println("该文件不存在!");
        }

//        for(int i=1;i<=100;i++) {
//            File file = new File("./test"+i+".txt");
//            if (file.exists()) {
//                file.delete();
//                System.out.println("该文件已删除!");
//            } else {
//                System.out.println("该文件不存在!");
//            }
//        }
    }
}
