package file;

import java.io.File;

/**
 * java.io.File
 * File的每一个实例用于表示硬盘上的一个文件或目录(实际表示的仅为一个抽象路径)
 * 使用File可以:
 * 1:访问其表示的文件或目录的属性信息(名字，大小，修改日期等等)
 * 2:创建，删除文件或目录
 * 3:访问一个目录中的子项
 *
 * 但是File不能访问文件的内容！(java其他的类有该功能)
 */
public class FileDemo {
    public static void main(String[] args) {
        //访问当前项目目录下的demo.txt
        //File:文件
//        File file = new File("C:/Users/X/IdeaProjects/JSD2204SE/demo.txt");
        File file = new File("./demo.txt");

        //获取File对象表示的文件或目录的名字
        String name = file.getName();
        System.out.println("名字:"+name);
        //获取长度，这里返回值为long类型。
        long length = file.length();//file表示的文件的长度(单位是字节)
        System.out.println("长度:"+length+"个字节");
        /*
            boolean canRead() 是否可读
            boolean canWrite()是否可写
         */
        boolean cr = file.canRead();
        boolean cw = file.canWrite();
        System.out.println("可读:"+cr);
        System.out.println("可写:"+cw);

        boolean ih = file.isHidden();
        System.out.println("是否是隐藏的:"+ih);
    }
}
