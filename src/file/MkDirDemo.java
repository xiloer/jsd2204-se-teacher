package file;

import java.io.File;

/**
 * 创建一个目录
 */
public class MkDirDemo {
    public static void main(String[] args) {
        /*
            1:创建一个File对象表示要创建的目录
            2:判断该目录是否存在
            3:如果目录不存在，则创建它
         */
        //在当前目录下新建一个目录:demo
        File dir = new File("./demo");
        if(dir.exists()){
            System.out.println("该目录已经存在了");
        }else{
            /*
                make:制作
                directory:目录(文件夹)
                File提供的方法:
                mkdir()将当前File表示的目录真实创建出来
             */
            dir.mkdir();
            System.out.println("该目录已创建!");
        }
    }
}
