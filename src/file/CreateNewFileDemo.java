package file;

import java.io.File;
import java.io.IOException;

/**
 * 创建一个新的文件
 */
public class CreateNewFileDemo {
    public static void main(String[] args) throws IOException {
        //在当前项目目录下新建一个文件:test.txt
        /*
            1:创建一个File对象，并指定好该文件对应的路径
            2:判断该文件是否已经真实存在了
            3:如果文件不存在，则将其创建出来
         */
        //1
        File file = new File("./test.txt");
//        File file = new File("./abc/test.txt");//由于test.txt所在的目录abc不存在，那么下面创建时会报错
        //2
        /*
            boolean exists()
            判断当前File对象表示的路径对应的位置是否已经真实存在
            该文件或目录了，如果存在则返回值为true否则为false
         */
        if(file.exists()){
            System.out.println("该文件已经存在了!");
        }else{
            /*
                createNewFile
                创建  新  文件

                创建文件有前提条件:
                该文件所在的目录必须真实存在，否则创建时会抛出异常:java.io.IOException 系统找不到指定的文件
             */
            file.createNewFile();//alt+enter 选择add exception....先将异常抛出
            System.out.println("文件已创建");
        }
    }
}
