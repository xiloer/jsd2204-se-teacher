package file;

import java.io.File;
import java.io.FileFilter;

/**
 * 有条件的获取一个目录中的子项
 */
public class ListFilesDemo2 {
    public static void main(String[] args) {
        //需求:获取当前目录中名字里含有"e"的所有子项
        /*
            Filter:过滤器
            accept:接受
         */
        FileFilter filter = new FileFilter() {
            /*
                实现过滤器接口就必须重写accept方法。该方法的作用是定义该过滤器
                接受条件。
                参数file可以理解为是经过当前过滤器的一个file对象，如果过滤器认为
                它符合过滤器要求则accept方法应当返回true，否则返回false。

                例如:我们本案例的需求是获取名字包含"e"的子项。那么我们定义过滤器的
                过滤规则时就是当file表示的子项名字里包含字母"e"时accept方法就应当
                返回true，否则返回false.
             */
            public boolean accept(File file) {
                //获取经过当前过滤器的file表示的文件或目录的名字
                String fileName = file.getName();
                /*
                    return fileName.matches(".*e.*");//正则表达式实现

                    return fileName.indexOf("e")!=-1;//使用indexOf实现

                    补充:字符串提供的方法:boolean contains(String str)
                    作用是判断当前字符串是否包含参数指定的内容，包含则返回true
                    return fileName.contains("e");
                 */
                return fileName.contains("e");//contains:包含
            }
        };
        File dir = new File(".");
        if(dir.isDirectory()){
            File[] subs = dir.listFiles(filter);
            System.out.println("共获取子项"+subs.length+"个");
        }
    }
}
