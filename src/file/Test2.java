package file;

import java.io.File;
import java.io.FileFilter;

/**
 * 需求:
 * 获取当前目录中所有名字里含有字母o的子项
 */
public class Test2 {
    public static void main(String[] args) {
        FileFilter filter = new FileFilter() {
            public boolean accept(File file) {
                String fileName = file.getName();
                return fileName.contains("o");
            }
        };
        File dir = new File(".");
        if(dir.isDirectory()){
            File[] subs = dir.listFiles(filter);
            for(int i=0;i<subs.length;i++){
                System.out.println(subs[i].getName());
            }
        }

    }
}
