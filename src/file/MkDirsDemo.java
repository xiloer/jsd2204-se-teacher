package file;

import java.io.File;

/**
 * 创建多级目录
 */
public class MkDirsDemo {
    public static void main(String[] args) {
        //在当前目录下创建:a/b/c/d/e/f
        File dir = new File("./a/b/c/d/e/f");
        if(dir.exists()){
            System.out.println("该目录已存在");
        }else{
//            dir.mkdir();//mkdir创建目录时要求该目录所在的目录必须存在
            dir.mkdirs();//mkdirs在创建目录时会将所有不存在的父目录一同创建出来(开发推荐使用)
            System.out.println("该目录已创建!");
        }
    }
}
