package file;

import java.io.File;

/**
 * 获取一个目录中的所有子项
 */
public class ListFilesDemo {
    public static void main(String[] args) {
        //获取当前项目目录下的所有子项
        /*
            相对路径中:"."表示当前目录，在IDEA中执行程序是这个"."表示
            的就是当前项目目录。
         */
        File dir = new File(".");
        /*
            boolean isFile()
            用来判断当前File对象表示的是否为一个文件

            boolean isDirectory()
            用来判断当前File对象表示的是否为一个目录
         */
        if(dir.isDirectory()){//获取一个目录中所有子项的前提就是File当前表示的是一个目录
            /*
                File[] listFiles()
                获取当前File对象表示的目录中的所有子项。返回的File数组中每一个元素表示
                这个目录中的一个子项。

                list:列表
                listFiles:理解为:列出所有子项
             */
            File[] subs = dir.listFiles();
            System.out.println("当前目录一共包含"+subs.length+"个子项");
            for(int i=0;i<subs.length;i++){
                File sub = subs[i];
                System.out.println("名字:"+sub.getName());
            }
        }

    }
}
