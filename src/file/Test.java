package file;

import java.io.File;
import java.io.IOException;

/**
 * 在当前目录下新建100个文件。文件名为:test1.txt---test100.txt
 */
public class Test {
    public static void main(String[] args) throws IOException {
        /*
            思路:首先要用for循环进行100次。创建File时指定的路径中，
            可以用循环变量i拼接到文件名中达到目的
         */
        for(int i=1;i<=100;i++) {
            File file = new File("./test"+i+".txt");
            if (file.exists()) {
                System.out.println("该文件已经存在了!");
            } else {
                file.createNewFile();
                System.out.println("文件已创建");
            }
        }
    }
}
