package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * 聊天室服务端
 */
public class Server_v2 {
    /*
        java.net.ServerSocket 运行在服务端的ServerSocket主要有两个作用
        1:打开服务端口，客户端就是通过这个端口与服务端建立连接的。本案例打开的就是8088端口
        2:监听服务端口(8088),一旦一个客户端通过该端口与服务端请求建立连接，此时会立即返回
          一个Socket实例，服务端就通过该实例与该客户端交互。

        将ServerSocket比喻为"总机"。客户端的"电话"都是先拨通到"总机"上，然后"总机"分配一台"电话"，
        通过这个"电话"就可以和该客户端沟通了
     */
    private ServerSocket serverSocket;

    public Server_v2(){
        try {
            System.out.println("正在启动服务端，打开8088端口...");
            /*
                这里实例化ServerSocket的同时我们要指定服务端口，客户端就是通过该端口连接的
                注意:这个端口不能与操作系统上其他应用程序已经打开的端口重复，否则会抛出异常:
                java.net.BindException:address already in use
                         绑定异常       地址     已经     被使用
                解决办法:更换端口，直到可用。
                       或者杀死之前运行的服务端程序.
             */
            serverSocket = new ServerSocket(8088);
            System.out.println("服务端启动完毕!");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void start(){
        try {
            while(true) {
                System.out.println("等待客户端连接...");
                /*
                ServerSocket提供的一个重要方法:
                Socket accept()
                该方法用于接收客户端的连接，该方法是一个阻塞方法，调用后程序会"卡住"，直到一个
                客户端连接了ServerSocket,此时该方法会立即返回一个Socket实例。服务端通过该
                实例就可以与连接的客户端交互了。
                该方法理解为就是总机的"接电话"动作。
             */
                Socket socket = serverSocket.accept();
                System.out.println("一个客户端连接了!");
                //启动一个线程负责与该客户端交互
                ClientHandler clientHandler = new ClientHandler(socket);
                Thread t = new Thread(clientHandler);
                t.start();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        Server_v2 server = new Server_v2();
        server.start();
    }

    /**
     * 该线程任务负责与指定的客户端进行交互
     */
    private class ClientHandler implements Runnable{
        private Socket socket;
        public ClientHandler(Socket socket){
           this.socket = socket;
        }
        public void run() {
            try {
                /*
                Socket一个重要方法:
                InputStream getInputStream()
                通过该方法获取的字节输入流可以读取来自远端计算机发送过来的数据
             */
                InputStream in = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(in, StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr);
                /*
                    如果客户端异常断开，服务端会抛出异常
                    java.net.SocketException: Connection reset
                    该异常无法避免，因为这个是客户端的操作导致的。
                 */
                String line;
                while ((line = br.readLine()) != null) {
                    System.out.println("客户端说:" + line);
                }
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}
