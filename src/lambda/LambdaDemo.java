package lambda;

import java.io.File;
import java.io.FileFilter;

/**
 * lambda表达式是JDK8之后推出的一个新特性
 *
 * 用更精简的语法创建匿名内部类
 * lambda表达式语法:
 * (参数列表)->{
 *     方法体
 * }
 * 前提条件:当我们实现的接口中【只有一个抽象方法】时，才能使用lambda表达式形式创建
 *
 */
public class LambdaDemo {
    public static void main(String[] args) {
        //匿名内部类写法
        FileFilter filter = new FileFilter() {
            public boolean accept(File file) {
                return file.getName().contains("o");
            }
        };

        FileFilter filter1 = (File file)->{
                return file.getName().contains("o");
        };

        //特点1:方法参数的类型可以忽略不写
        FileFilter filter2 = (file)->{
            return file.getName().contains("o");
        };

        //特点2:如果方法体中只有一句代码时，方法体的"{}"可以忽略不写，如果这句代码包含
        //return关键字，那么return关键字也要一同被忽略!
        FileFilter filter3 = (file)->file.getName().contains("o");

        //特点3:如果方法的参数只有1个，那么参数列表的"()"可以忽略不写
        FileFilter filter4 = file->file.getName().contains("o");
    }
}
