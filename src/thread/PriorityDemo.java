package thread;

/**
 * 线程的优先级
 * 当我们调用一个线程的start方法后,线程就纳入到线程调度器程序(JVM内部)中被统一管理
 * 线程调度器会尽可能均匀分配时间片给每个线程,使他们在短时间内走走停停,造就同时执行的
 * 效果.
 * 线程纳入调度器后不能主动的索取时间片,只能被动分配.我们可以通过调整线程的优先级来
 * 影响一个线程获取时间片的概率.
 * 线程优先级越高的线程获取时间片的次数就越多.
 * 线程的优先级有10个等级,分别对应整数1-10
 * 其中1为最小优先级,10为最高优先级.5为默认值
 */
public class PriorityDemo {
    public static void main(String[] args) {
        Thread min = new Thread(){
            public void run(){
                for(int i=0;i<10000;i++){
                    System.out.println("min");
                }
            }
        };
        Thread max = new Thread(){
            public void run(){
                for(int i=0;i<10000;i++){
                    System.out.println("max");
                }
            }
        };
        Thread norm = new Thread(){
            public void run(){
                for(int i=0;i<10000;i++){
                    System.out.println("nor");
                }
            }
        };
//        min.setPriority(1);
//        max.setPriority(10);
        min.setPriority(Thread.MIN_PRIORITY);
        max.setPriority(Thread.MAX_PRIORITY);
        min.start();
        norm.start();
        max.start();

    }
}
