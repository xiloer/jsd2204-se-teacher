package thread;

/**
 * 有效的缩小同步范围可以在保证并发安全的前提下尽可能的提高并发效率
 * 同步块
 * synchronized(同步监视器对象){
 *     需要多个线程同步执行的代码片段
 * }
 */
public class SyncDemo2 {
    public static void main(String[] args) {
        //如果t1,t2分别执行shop1和shop2的buy方法时，该方法中的同步块不应当让他们排队，因为没有抢的问题
//        Shop shop1 = new Shop();
//        Shop shop2 = new Shop();
        Shop shop = new Shop();
        Thread t1 = new Thread("范传奇"){
            public void run(){
//                shop1.buy();
                shop.buy();
            }
        };
        Thread t2 = new Thread("王克晶"){
            public void run(){
//                shop2.buy();
                shop.buy();
            }
        };
        t1.start();
        t2.start();
    }
}

class Shop{
//    public synchronized void buy(){//买衣服方法
    public void buy(){
        try {
            Thread t = Thread.currentThread();//获取运行该方法的线程
            System.out.println(t.getName()+":正在挑衣服...");
            Thread.sleep(5000);
            /*
                使用同步块时需要在小括号中指定一个同步监视器对象.
                该对象的语法要求:
                它可以是任何引用类型的实例，只要保证一点，多个需要同步执行该
                代码片段的线程看到的必须是【同一个对象】即可。
                实际开发中可结合实际情况选择该对象。
             */
            synchronized (this) {
            /*
                字面量由于全局唯一，也不是好的锁对象，因为在不需要排队的场合也要排队了
                比如有两个shop对象，两个线程各自执行一个shop对象的buy方法，这相当于
                两个人进了两个不同的点，本身是没有"抢"试衣间问题的，但是由于看到的锁对象
                也相同，他们依然排队，这导致性能低下。
             */
//            synchronized ("hello") {
            //t不可以，因为每个线程执行buy方法是t就是该线程，因此看到的不是同一个对象
//            synchronized (t) {
            //记住:指定同步监视器对象是不能使用new这样的实例化动作，一定没有效果!
//            synchronized (new Object()) {
                System.out.println(t.getName() + ":正在试衣服...");
                Thread.sleep(5000);
            }

            System.out.println(t.getName()+":结账离开");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}