package thread;

/**
 * 静态方法上如果使用synchronized，那么该方法一定具有同步效果。
 */
public class SyncDemo3 {
    public static void main(String[] args) {
//        Boo b1 = new Boo();
//        Boo b2 = new Boo();
        Thread t1 = new Thread(){
            public void run(){
//                b1.dosome();
                Boo.dosome();
            }
        };
        Thread t2 = new Thread(){
            public void run(){
//                b2.dosome();
                Boo.dosome();
            }
        };
        t1.start();
        t2.start();
    }
}
class Boo{
    /*
        对于成员方法而言，在方法上直接使用synchronized时，锁对象就是当前方法
        的所属对象，即:this

        对于静态方法而言，在方法上直接使用synchronized时，锁对象是当前类的类对象
        注:
        类对象:Class的一个实例。
        JVM在加载一个类时，会同时实例化一个Class实例与之对应，因此每个被加载的类
        都有且只有一个Class实例，而这个实例就称为这个加载的类的类对象。
        类对象具体的知识会在反射知识点时详细说明。
     */
//    public synchronized void dosome(){
//    public synchronized static void dosome(){
    public static void dosome(){
        /*
            在静态方法中如果使用同步块时，那么指定的同步监视器对象通常也是使用
            当前类的类对象，写法为:类名.class
         */
        synchronized (Boo.class) {
            Thread t = Thread.currentThread();
            System.out.println(t.getName() + ":正在执行dosome方法...");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(t.getName() + ":执行dosome方法完毕!");
        }
    }
}



