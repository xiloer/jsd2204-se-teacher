package thread;

/**
 * 第二种创建线程的方式:
 * 实现Runnable接口单独定义线程任务.
 */
public class ThreadDemo2 {
    public static void main(String[] args) {
        //1先实例化要让线程并发执行的任务(Runnable)
        Runnable r1 = new MyRunnable1();
        Runnable r2 = new MyRunnable2();

        //2创建两条线程来分别执行这两个任务
        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);

        //3启动线程
        t1.start();
        t2.start();

    }
}

class MyRunnable1 implements Runnable{
    public void run() {
        for(int i=0;i<1000;i++){
            System.out.println("你是谁啊?");
        }
    }
}
class MyRunnable2 implements Runnable{
    public void run() {
        for(int i=0;i<1000;i++){
            System.out.println("开门!查水表的!");
        }
    }
}