package thread;

/**
 * 获取线程相关信息的一组方法(了解即可)
 */
public class ThreadInfoDemo {
    public static void main(String[] args) {
        //获取主线程
        Thread main = Thread.currentThread();
        //获取线程名字
        String name = main.getName();
        System.out.println("name:"+name);
        //id:唯一标识  使用该值可以唯一确定一个事物.
        // 可以作为唯一标识使用的值需要同时满足两个条件:非空且唯一
        long id = main.getId();
        System.out.println("id:"+id);

        //线程是否还活着
        boolean isAlive = main.isAlive();
        //线程是否为守护线程
        boolean isDaemon = main.isDaemon();
        //线程是否被中断了
        boolean isInterrupted = main.isInterrupted();
        System.out.println("是否活着:"+isAlive);
        System.out.println("是否为守护线程:"+isDaemon);
        System.out.println("是否被中断了:"+isInterrupted);

        //线程的优先级 1-10,线程默认的优先级都是5
        int priority = main.getPriority();
        System.out.println("优先级:"+priority);

    }
}
