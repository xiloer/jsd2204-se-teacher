package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池
 * 主要解决两个问题:
 * chong
 * 1:重用线程(避免线程的频繁创建和销毁所带来的不必要的系统开销)
 * 2:控制线程数量(线程过多会造成过多的内存消耗，以及CPU的过度切换，降低并发性能)
 */
public class ThreadPoolDemo {
    public static void main(String[] args) {
        //创建一个2个线程的线程池
        ExecutorService threadPool
                = Executors.newFixedThreadPool(2);

        for(int i=0;i<5;i++){
            Runnable r = new Runnable() {
                public void run() {
                    Thread t = Thread.currentThread();//获取执行当前任务的线程
                    System.out.println(t.getName()+":正在执行任务...");
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                    }
                    System.out.println(t.getName()+":执行任务完毕!");
                }
            };
            threadPool.execute(r);//将任务交给线程池
            System.out.println("交给线程池一个任务!");
        }


//        threadPool.shutdown();
        threadPool.shutdownNow();
        System.out.println("线程池关闭了");
    }
}











