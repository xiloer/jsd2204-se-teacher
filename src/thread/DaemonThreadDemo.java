package thread;

/**
 * 守护线程
 * 守护线程是通过用户线程(又称为普通线程或前台线程)调用setDaemon(true)方法设置而来
 * 的.实际上所有线程创建时默认都是用户线程.
 * 守护线程与用户线程的一个区别是:当一个进程结束时,所有正在运行的守护线程会被强制杀死
 * 进程的结束:
 * 当一个java进程中所有的用户线程都结束时,进程就会结束,此时它会强制杀死所有正在运行
 * 的守护线程.
 *
 * GC就是运行在守护线程上的
 *
 */
public class DaemonThreadDemo {
    public static void main(String[] args) {
        Thread rose = new Thread(){
            public void run(){
                for(int i=0;i<10;i++){
                    System.out.println("rose:let me go!");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("rose:啊啊啊啊啊啊AAAAAAaaaaaa....");
                System.out.println("扑通!");
            }
        };

        Thread jack = new Thread(){
            public void run(){
                while(true){
                    System.out.println("jack:妮儿~掰跳呗~这海水可凉了~");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        rose.start();
        /*
            设置守护线程的操作必须在线程启动前进行!(必选在start调用前完成)
         */
        jack.setDaemon(true);//设置为守护线程
        jack.start();


//        while(true);
//        System.out.println("main方法执行完毕了,意味着主线程结束了!");
    }
}
