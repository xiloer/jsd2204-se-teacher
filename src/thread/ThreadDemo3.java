package thread;

/**
 * 使用匿名内部类的形式完成线程的两种创建.
 */
public class ThreadDemo3 {
    public static void main(String[] args) {
        //继承Thread重写run方法的形式
        Thread t1 = new Thread(){
            public void run(){
                for(int i=0;i<1000;i++){
                    System.out.println("你是谁啊!");
                }
            }
        };
        //实现Runnable接口单独定义线程任务的方法
//        Runnable r2 = new Runnable(){
//            public void run(){
//                for(int i=0;i<1000;i++){
//                    System.out.println("我是查水表的!");
//                }
//            }
//        };
        Runnable r2 = ()->{
                for(int i=0;i<1000;i++){
                    System.out.println("我是查水表的!");
                }
        };
        Thread t2 = new Thread(r2);

        t1.start();
        t2.start();
    }
}




