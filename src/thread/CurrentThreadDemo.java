package thread;

/**
 * java中所有的代码都是靠线程执行的,main方法也不例外.实际上java进程启动后会将虚拟机
 * 跑起来,而虚拟机启动后会创建一条线程来执行main方法,该线程有一个名字叫"main",因此
 * 我们称它为"主线程".但是该线程与我们创建的线程并无区别.
 */
public class CurrentThreadDemo {
    public static void main(String[] args) {
        /*
            current:当前的
            Thread:线程

            线程提供了一个静态方法:
            static Thread currentThread()
            作用:该方法会返回运行这个方法的线程
         */
        Thread main = Thread.currentThread();
        /*
            线程重写toString方法,格式如下:
            Thread[线程名字,线程优先级,所在的线程组]
            例如主线程的输出:
            Thread[main,5,main]
         */
        System.out.println("主线程:"+main);
        dosome();//让主线程执行dosome方法
    }

    public static void dosome(){
        //获取运行dosome方法的线程
        Thread t = Thread.currentThread();
        System.out.println("执行dosome方法的线程是:"+t);
    }
}


