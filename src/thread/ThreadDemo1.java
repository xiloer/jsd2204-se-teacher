package thread;

/**
 * 多线程
 * 多线程是并发执行代码的,可以解决代码之前的"牵绊"问题.
 * 并发执行:代码之间在微观上是走走停停的(CPU在这些代码片段之间反复切换执行),宏观上
 * 给我们的感受是"同时"执行的效果.
 *
 * Thread类是线程类,我们实际开中使用线程时有两种创建方式
 * 方式1:
 * 定义一个类并继承Thread,然后重写run方法,在run方法中定义线程要执行的任务.
 */
public class ThreadDemo1 {
    public static void main(String[] args) {
        Thread t1 = new MyThread1();
        Thread t2 = new MyThread2();
        //启动线程不能调用run方法!!而是调用start方法!
        /*
            start方法是将当前线程纳入到线程调度器中,该线程才会开始并发执行.
            并且当start方法执行完毕后,run方法会很快被自动执行(当该线程第一次
            获取CPU时间片后会开始自动执行自身的run方法来开始工作).
         */
        t1.start();
        t2.start();
    }
}

/**
 * 第一种创建线程的方式优点:结构简单,利于使用匿名内部类形式创建
 * 缺点:
 * 1:有继承冲突问题,由于java是单继承的,这导致如果继承了Thread则不能再继承其它类
 *   去复用方法了
 * 2:我们重写了线程的run方法,并在其中定义了线程要执行的任务,此时该线程就与任务存在
 *   了一个必然的耦合关系.这不利于线程的重用.
 */
class MyThread1 extends Thread{
    public void run(){
        for(int i=0;i<1000;i++){
            System.out.println("你是谁啊?");
        }
    }
}
class MyThread2 extends Thread{
    public void run(){
        for(int i=0;i<1000;i++){
            System.out.println("开门!查水表的!");
        }
    }
}
