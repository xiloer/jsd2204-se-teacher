package io;

public class Test2 {
    public static void main(String[] args) {
        /*
            获取的是当前系统时间的毫秒值。
            该毫秒值是根据1970年1月1日 00:00:00到当前系统时间所经过的毫秒
            UTC时间，世界协调时
         */
        long time = System.currentTimeMillis();
        System.out.println(time);
    }
}
