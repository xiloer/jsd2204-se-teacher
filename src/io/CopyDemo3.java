package io;

import java.io.*;

/**
 * 缓冲流(是一对高级流)
 * java.io.BufferedInputStream和BufferedOutputStream
 * 同样也继承自:java.io.InputStream和OutputStream。所以它们也有对应的read,write方法。
 *
 * 缓冲流的功能:
 * 在流连接中发挥的作用是提高读写数据的效率
 */
public class CopyDemo3 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("01.rmvb");
//        BufferedInputStream bis = new BufferedInputStream(fis);//默认缓冲区大小8Kb
        BufferedInputStream bis = new BufferedInputStream(fis,1024*10);//自行执行缓冲区大小
        FileOutputStream fos = new FileOutputStream("01_cp.rmvb");
//        BufferedOutputStream bos = new BufferedOutputStream(fos);
        BufferedOutputStream bos = new BufferedOutputStream(fos,1024*10);
        int d;
        long start = System.currentTimeMillis();//开始时间
        /*
            缓冲流内部维护一个字节数组，默认长度为8Kb
            因此通过缓冲流读写数据时，缓冲流一定将读写方式转换为块读写进行，来保证读写效率
         */
        while((d = bis.read())!=-1){
            bos.write(d);
        }
        long end = System.currentTimeMillis();//结束时间

        System.out.println("复制完毕，耗时:"+(end-start)+"ms");
        bis.close();
        bos.close();
    }
}
