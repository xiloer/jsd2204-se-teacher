package io;

import java.io.*;

/**
 * 使用对象输入流进行对象的反序列化
 */
public class OISDemo {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //将person.obj文件中保存的对象反序列化回来
        FileInputStream fis = new FileInputStream("person.obj");
        ObjectInputStream ois = new ObjectInputStream(fis);
        /*
            对象输入流提供了反序列化操作
            Object readObject()
            反序列化后的对象都是以Object类型返回的，所以实际接收后要向下造型得到实际的原型。
            向下造型(强制类型转换)
         */
        Person p = (Person)ois.readObject();
        System.out.println(p);

        ois.close();
    }
}
