package io;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 缓冲流的缓冲区问题
 */
public class FlushDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("bos.txt");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        String line = "一给我里giao~";
        byte[] data = line.getBytes(StandardCharsets.UTF_8);
        bos.write(data);
        /*
            flush:冲水的"冲"
            将当前缓冲流中缓冲区中已经缓存的数据一次性写出

            flush方法不是缓冲流独有的方法，实际上所有的字节输出流都有该方法!
            java.io.OutputStream实现了java.io.Flushable接口，该接口上定义着flush方法。
            由于所有字节输出流都继承自OutputStream,因此所有的字节输出流都有flush方法。
            但是绝大部分的高级流中的flush方法实现都是调用其连接的流的flush方法，目的是将flush动作
            传递下去，直到调用到缓冲字节输出流的flush为止，而缓冲字节输出流的flush是真实做清空缓冲
            区数据做强制写出操作的。

         */
        bos.flush();
        System.out.println("写出完毕");

        bos.close();
    }
}







