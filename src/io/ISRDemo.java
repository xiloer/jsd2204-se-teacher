package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * 测试转换流读取字符的基础操作
 */
public class ISRDemo {
    public static void main(String[] args) throws IOException {
        //osw.txt文件中的所有字符读取回来
        FileInputStream fis = new FileInputStream("osw.txt");
        InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
        /*
            java.io.Reader字符输入流的超类
            int read():一次读取一个字符，返回的int值实际是一个char。但是如果int值表示的是-1则表达读取到了末尾
            int read(char[] data):块读文本操作
         */
//        int d = isr.read();//读取文件中的第一个字符
//        System.out.println((char)d);

        int d;
        while((d = isr.read()) != -1){
            System.out.print((char)d);
        }
        isr.close();
    }
}
