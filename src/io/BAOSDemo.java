package io;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * java.io.ByteArrayOutputStream
 * 字节数组输出流，这是一个低级流。该流连接的是一个字节数组(该字节数组就在这个流自己
 * 内部)。通过这个流写出的字节数据最终都保存在内部的字节数组中。
 */
public class BAOSDemo {
    public static void main(String[] args) {
        //字节数组输出流，低级流。通过它写出的数据全部转存在内部的byte数组中
        /*
            这个低级流不强制要求我们处理IO异常，因为它并不会对其他硬件设备进行
            访问。只是将写出的数据存在内部的字节数组中了。
         */
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //转换流，字符流，高级流。1:衔接字节与字符流 2:将写出的字符转换为字节
        OutputStreamWriter osw = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        //缓冲字符输出流，字符流，高级流。块写文本数据加速的。
        BufferedWriter bw = new BufferedWriter(osw);
        //PW，字符流，高级流。1:按行写出字符串 2:自动的行刷新
        PrintWriter pw = new PrintWriter(bw,true);

        pw.println("你好");

        //当写出完毕后，我们可以通过baos获取刚刚写出的所有字节
        byte[] data = baos.toByteArray();
        System.out.println("字节总数:"+data.length);
        System.out.println(Arrays.toString(data));
    }
}


