package io;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 使用当前类测试对象流的对象序列化操作和反序列化操作
 */
public class Person implements Serializable {
    /*
        通常我们实现Serializable接口后就要主动定义序列化版本号
        版本号的值可以是任意数字，固定不会改变即可。
        如果不指定，序列化对象时则使用默认的版本号，但是该版本号会随着类的改变而改变。
        序列化版本号发生了改变会影响反序列化。
        当对象输入流在进行反序列化时发现这组字节表示的对象与其所属类当前定义的版本号不一致
        时会抛出异常。
     */
    public static final long serialVersionUID = 1L;
    private String name;//名字
    private int age;//年龄
    private String gender;//性别
    /*
        transient关键字修饰的属性在进行对象序列化时该值会被忽略。
        忽略不必要的属性可以到达对象序列化"瘦身"的目的。减少资源开销。
     */
    private transient String[] otherInfo;//其他信息

    //alt+insert 自动生成构造方法，get set方法
    public Person(String name, int age, String gender, String[] otherInfo) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.otherInfo = otherInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String[] getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String[] otherInfo) {
        this.otherInfo = otherInfo;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", otherInfo=" + Arrays.toString(otherInfo) +
                '}';
    }
}
