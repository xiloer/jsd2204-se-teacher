package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 将小写字母a-z写入文件code.txt中
 */
public class Test1 {
    public static void main(String[] args) throws IOException {
        /*
            在asc编码中小写字母'a'的2进制与整数97的2进制一致。
         */
        FileOutputStream fos = new FileOutputStream("code.txt");
        for(int i=0;i<26;i++){
            fos.write(97+i);
        }
        System.out.println("写出完毕!");
        fos.close();
    }
}
