package map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * JDK8之后Map和集合都支持使用lambda表达式遍历的操作
 */
public class ForEachDemo {
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        c.add("six");
        System.out.println(c);
        for(String e : c){
            System.out.println(e);
        }
        c.forEach(e -> System.out.println(e));
//        c.forEach(System.out::println);

        Map<String,Integer> map = new HashMap<>();
        map.put("语文",98);
        map.put("数学",97);
        map.put("英语",96);
        map.put("物理",95);
        map.put("化学",98);
        System.out.println(map);
        map.forEach((k,v)-> System.out.println(k+":"+v));
    }
}
