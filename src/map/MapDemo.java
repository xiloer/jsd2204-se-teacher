package map;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Map 查找表
 * Map是常用的数据结构，体现的样子是一个多行两列的表格。其中左列称为key，右列
 * 称为value。
 * map总是根据key获取对应的value。
 * map有一个要求:key不允许重复！！ (重复是依靠key的equals判定的)
 *
 * java.util.Map是一个接口，常用的实现类:
 * java.util.HashMap:散列表(使用散列算法实现的Map，当今查询速度最快的数据结构)
 *
 */
public class MapDemo {
    public static void main(String[] args) {
//        Map<String,Integer> map = new HashMap<>();
        Map<String,Integer> map = new LinkedHashMap<>();
        /*
            V put(K k,V v)
            将一组键值对存入Map
            由于Map中key不允许重复，所以如果使用重复的key存入value时，则是替换
            value操作，此时方法返回值就是被替换的value。如果没有替换则返回null。
         */
        Integer value = map.put("语文",98);
        /*
            注意:如果Map的value是包装类的类型时，接受返回值避免用对应的基本
            类型，因为该方法返回值可能为null，这会导致因为自动拆箱引发空指针异常!
         */
//        int value = map.put("语文",98);//不要这样做!
        System.out.println(value);
        map.put("数学",97);
        map.put("英语",96);
        map.put("物理",95);
        map.put("化学",98);
        System.out.println(map);

        //会将“数学”之前的97替换并返回。
        value = map.put("数学",60);
        System.out.println(map);
        System.out.println(value);

        /*
            V get(Object key)
            根据给定的key获取对应的value，如果给定的key不存在返回值为null
         */
        value = map.get("英语");
        System.out.println("英语:"+value);
        value = map.get("体育");
        System.out.println("体育:"+value);

        int size = map.size();
        System.out.println("size:"+size);

        /*
            boolean containsKey(Object key)
            boolean containsValue(Object value)
            判断Map中是否含有给定的key或value
         */
        boolean ck = map.containsKey("英语");
        System.out.println("包含key:"+ck);

        boolean cv = map.containsValue(95);
        System.out.println("包含value:"+cv);

        /*
            V remove(Object key)
            根据给定的key删除这组键值对，返回值为这个key对应的value
            如果给定的key不存在则返回值为null。
         */
        value = map.remove("英语");
        System.out.println(map);
        System.out.println(value);
    }
}






