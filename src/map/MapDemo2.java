package map;

import java.util.*;

/**
 * Map的遍历
 * Map提供了三种遍历方式:
 * 1:遍历所有的key
 * 2:遍历每一组键值对
 * 3:遍历所有的value(该操作不常用)
 */
public class MapDemo2 {
    public static void main(String[] args) {
        Map<String,Integer> map = new HashMap<>();
        map.put("语文",98);
        map.put("数学",97);
        map.put("英语",96);
        map.put("物理",95);
        map.put("化学",98);
        System.out.println(map);

        /*
            Set keySet()
            将当前Map中所有的key以一个Set集合形式返回。
         */
        Set<String> keySet = map.keySet();
        for(String key : keySet){
            System.out.println("key:"+key);
        }

        /*
            Set entrySet()
            该方法会将Map中每组键值对以一个Entry实例形式表示，并最终存入集合后
            将该集合返回。
            java.util.Map.Entry它用来表示Map中的一组键值对
         */
        Set<Map.Entry<String,Integer>> entrySet = map.entrySet();
        for(Map.Entry<String,Integer> e : entrySet){
            String key = e.getKey();
            Integer value = e.getValue();
            System.out.println(key+":"+value);
        }
        /*
            Collection values()
            将当前Map中所有的value以一个集合形式返回。
         */
        Collection<Integer> values = map.values();
        for(Integer value : values){
            System.out.println("value:"+value);
        }

    }
}






