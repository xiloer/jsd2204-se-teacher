package homework.day02;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 改正下面程序的错误
 * <p>
 * 程序实现的功能需求:复制一个文件
 *
 * @author Xiloer
 */
public class Test01 {
    public static void main(String[] args) throws IOException {
        //ctrl+alt+L 自动对齐代码
        //批量注释:鼠标选中需要注释的代码片段后按ctrl+/ 取消注释同样操作即可
        /*
            类名报错时通常情况:
            1:类名拼写错误
            2:类没有导包
         */
        //之前没有导包
        FileInputStream fis
                = new FileInputStream("test.txt");

        //这里应当用输出流向复制文件中写操作，再者实例化时类名拼写错误
//        FileInputStream fos
//                = new FIleInputStream("test_cp.txt");

        FileOutputStream fos
                = new FileOutputStream("test_cp.txt");

        int d;
        while ((d = fis.read()) != -1) {
            //复制时不能在一次循环中连续调用两次read()方法！
//            fos.write(fis.read());
            fos.write(d);
        }
        System.out.println("复制完毕!");
        fis.close();
        fos.close();
    }
}