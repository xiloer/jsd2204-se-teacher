package homework.day02;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 将当前目录下的所有文件都复制一份，复制的文件命名为:原
 * 文件名_cp.后缀
 * 比如原文件为:test.dat
 * 复制后的文件为:test_cp.dat
 *
 *
 * @author Xiloer
 *
 */
public class Test02 {
    public static void main(String[] args) {
        /*
            思路:
            1:获取到当前目录下的所有文件
            2:每个文件获取文件名，通过处理得到一个文件名_cp.后缀的新文件名
            3:创建文件输入流读取该文件，并创建文件输出流写入到复制文件
         */
        File dir = new File(".");
        /*
            boolean isFile()判断当前File表示的是否为一个文件
            boolean isDirectory()判断当前File表示的是否为一个目录
         */
        File[] subs = dir.listFiles(f->f.isFile());//过滤器的过滤条件是该元素必须表示的是一个文件
        for (int i=0;i<subs.length;i++){
            System.out.println(subs[i].getName());
            //根据当前文件名定义新的文件名
            /*
                例如:文件名test.1.1.2.txt
                将文件名从第一个字符开始截取到最后一个"."之前的内容:文件名部分
                从文件名最后一个"."开始截取到末尾:文件的后缀部分
             */
            String fileName = subs[i].getName();
            int index = fileName.lastIndexOf(".");//先找到文件名中最后一个"."的位置
            String name = fileName.substring(0,index);//文件名部分：test.1.1.2
            String ext = fileName.substring(index);//文件名后缀部分：.txt
            String newName = name+"_cp"+ext;//新文件名test.1.1.2_cp.txt
            System.out.println(newName);
            try(
                FileInputStream fis = new FileInputStream(subs[i]);//读取原文件
                FileOutputStream fos = new FileOutputStream(newName)//写入复制文件
            ){
                int len;
                byte[] data = new byte[1024*10];
                while((len = fis.read(data))!=-1){
                    fos.write(data,0,len);
                }
                System.out.println("复制完毕!");
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}
