package homework.day02;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * 简易记事本工具
 * 程序启动后，将用户在控制台输入的每一行字符串都写入到文件note.txt中。
 * 当用户输入"exit"时程序退出。
 */
public class Test03 {
    public static void main(String[] args) {
        try (
                FileOutputStream fos = new FileOutputStream("note.txt");
        ){
            Scanner scanner = new Scanner(System.in);
            System.out.println("请开始输入内容，单独输入exit是退出");
            while(true){
                String line = scanner.nextLine();
                if("exit".equals(line)){
                    break;
                }
                byte[] data = line.getBytes(StandardCharsets.UTF_8);
                fos.write(data);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
