package homework.day03;

import java.io.*;

/**
 * 改正下面程序的错误
 * 
 * 程序实现需求:使用缓冲流完成文件的复制操作
 * @author Xiloer
 *
 */
public class Test01 {
	public static void main(String[] args){
		try(
			FileInputStream fis = new FileInputStream("test.txt");
			//类名拼写错误
	//		BufferedInputStream bis = new BuffereddInputStream(fis);
			BufferedInputStream bis = new BufferedInputStream(fis);

			FileOutputStream fos = new FileOutputStream("test_cp.txt");
			//创建的流方向错误，应当创建为输出流
	//		BufferedInputStream bos = new BufferedOutputStream(fos);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
		) {
			int d = 0;
			/*
				循环条件应当是没有读取到末尾才复制
				不能用分支！要使用循环来复制
			 */
//			if ((d = bis.read()) == -1) {
			while ((d = bis.read()) != -1) {
				//方法名拼写错误
//				bos.wirte(d);
				bos.write(d);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		System.out.println("复制完毕!");
	}
}
