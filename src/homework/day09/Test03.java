package homework.day09;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * 生成10个0-100之间的不重复的随机数,并输出
 * @author Xiloer
 *
 */
public class Test03 {
	public static void main(String[] args) {
		Random random = new Random();
		Set<Integer> set = new HashSet<>();//Set集合不能放重复元素
		while(set.size()<10){
			set.add(random.nextInt(100));
		}
		System.out.println(set);
		System.out.println(set.size());
	}
}
