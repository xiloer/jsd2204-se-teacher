package homework.day09;

//import java.awt.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * 改错题
 * 
 * 程序需求:向一个List集合中添加10个数字1-10，并将它们
 * 遍历出来并输出到控制台。
 *
 * @author Xiloer
 *
 */
public class Test01 {
	public static void main(String[] args) {
		//1泛型不能用基本类型
		//2List是一个接口，不能实例化
		//3List导包错误，应当用的是java.util.List
//		List<int> list = new List<int>();
		List<Integer> list = new ArrayList<>();
		//缺少逻辑:向集合中添加10个数字
		for(int i=1;i<=10;i++){
			list.add(i);
		}
		//将迭代器指定泛型
//		Iterator it = list.iterator();
		Iterator<Integer> it = list.iterator();
		while(it.hasNext()) {
			int i = it.next();
			//不要在遍历中连续调用两次next()
//			System.out.println(it.next());
			System.out.println(i);
		}
	}
}





