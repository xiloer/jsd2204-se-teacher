package homework.day04;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * 改错
 * 
 * 程序实现的是简易记事本工具。程序启动后向pw.txt文件写内容
 * 用户输入的每一行字符串都写入到文件中，单独输入exit时
 * 程序退出。
 * 
 * @author Xiloer
 *
 */
public class Test02 {
	public static void main(String[] args) {
		try(
			//写文件的操作要使用文件输出流
//			FileInputStream fos = new FileInputStream("pw.txt");
			FileOutputStream fos = new FileOutputStream("pw.txt");
			/*
				UnsupportedEncodingException
				不支持的     字符集   异常
				引起该异常的原因是指定的字符集名字拼写错误.
			 */
//			OutputStreamWriter osw = new OutputStreamWriter(fos,"UFT-8");
			OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
			BufferedWriter bw = new BufferedWriter(osw);
			PrintWriter pw = new PrintWriter(bw,true);
		) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("请开始输入内容");
			while (true) {
				String str = scanner.nextLine();
				if ("exit".equals(str)) {
					break;
				}
				//变量名拼写错误
//				pw.println(srt);
				pw.println(str);
				//不应当在循环过程中调用流的关闭操作,应当是在流最终使用完毕后才关闭
//				pw.close();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
