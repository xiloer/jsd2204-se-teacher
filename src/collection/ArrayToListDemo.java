package collection;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 数组转换为List集合.
 * 数组的工具类:java.util.Arrays有一个静态方法asList,可以将一个数组转换为
 * 一个List集合.
 */
public class ArrayToListDemo {
    public static void main(String[] args) {
        String[] array = {"one","two","three","four","five"};
        System.out.println("array:"+ Arrays.toString(array));

        List<String> list = Arrays.asList(array);
        System.out.println("list:"+list);
        /*
            对该集合的操作就是对原数组的操作.同样的对数组进行了操作,集合也能看到
            改后的效果.
         */
        list.set(1,"six");
        System.out.println("list:"+list);
        System.out.println("array:"+ Arrays.toString(array));
        array[2] = "seven";
        System.out.println("array:"+ Arrays.toString(array));
        System.out.println("list:"+list);
        //由于数组是定长的,所以该集合任何会修改长度的操作都是不支持的,会抛出异常
//        list.add("!!!");
        /*
            如果希望对集合进行增删等操作,那么只能自行再创建一个集合并包含该集合
            中的所有元素
            所有的集合都支持一个参数类型为Collection的构造方法,作用是创建当前
            集合的同时包含给定集合中的所有元素.
         */
        List<String> list2 = new ArrayList<>(list);
        list2.add("!!!");
        System.out.println("list2:"+list2);
    }
}






