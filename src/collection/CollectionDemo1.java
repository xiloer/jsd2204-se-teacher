package collection;

import java.util.ArrayList;
import java.util.Collection;

/**
 * java集合框架
 * java.util.Collection接口  集合接口
 * 该接口是所有集合的顶级接口,规定了所有集合都必须具备的操作.
 *
 * 集合与数组一样,可以保存一组元素,并且对于元素的相关操作都提供成了方法,使用方便.
 * 集合有多种不同的数据结构可供使用.
 *
 * Collection下面有两个常见的子接口(常见分类)
 * java.util.List:可重复且有序的集合
 * java.util.Set:不可重复的集合
 * 所谓重复指的是元素是否重复,而重复的依据是元素自身equals方法比较是否为true
 * 如果为true则认为是重复元素,Set集合不允许存入重复的元素.
 *
 */
public class CollectionDemo1 {
    public static void main(String[] args) {
        Collection c = new ArrayList();
        /*
            boolean add(E e)
            向当前集合中添加一个元素
            注意:集合只能存放引用类型元素!!!!
            返回值表示该元素是否成功添加到集合中,如果成功添加则返回true否则为false
         */
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
//        c.add(123);//触动自动装箱特性,变成Integer(引用类型)
        System.out.println(c);
        /*
            int size()
            返回的是当前集合的元素个数
         */
        int size = c.size();
        System.out.println("size:"+size);

        /*
            boolean isEmpty()
            判断当前集合是否为一个空集
            当且仅当size=0时,isEmpty返回值为true
         */
        boolean isEmpty = c.isEmpty();
        System.out.println("是否为空集:"+isEmpty);

        /*
            void clear()
            清空集合
         */
        c.clear();
        System.out.println(c);
        System.out.println("size:"+c.size());
        System.out.println("isEmpty:"+c.isEmpty());
    }
}



