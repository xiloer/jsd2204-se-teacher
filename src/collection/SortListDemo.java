package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * 集合的排序
 * java.util.Collections是集合的工具类,提供了静态方法sort可以对List集合进行
 * 自然排序(从小到大)
 */
public class SortListDemo {
    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<10;i++){
            list.add(random.nextInt(100));
        }
        System.out.println(list);
//        Collections.sort(list);
        //降序
        Collections.sort(list,(i1,i2)->i2-i1);

        System.out.println(list);

    }
}



