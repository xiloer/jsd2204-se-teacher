package collection;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 元素equals方法会影响集合的一些操作.比如判断是否为重复元素.还有包含,删除元素等操作
 * 也是依靠该方法的.
 */
public class CollectionDemo2 {
    public static void main(String[] args) {
        Collection c = new ArrayList();
        c.add(new Point(1,2));
        c.add(new Point(3,4));
        c.add(new Point(5,6));
        c.add(new Point(7,8));
        c.add(new Point(9,10));
        c.add(new Point(1,2));
        /*
            当我们输出一个集合时,实际输出的是该集合toString方法的返回值
            集合重写了toString方法,格式为:
            [元素1.toString(), 元素2.toString(), ...]
         */
        System.out.println(c);

        /*
            contains:包含
            boolean contains(Object o)
            判断当前集合是否包含给定元素,若包含则返回true

            判断包含的标准:当前集合里是否含有与给定元素equals比较true的元素.
            如果存在则认为包含.
         */
        Point p = new Point(1,2);
        boolean contains = c.contains(p);
        System.out.println("包含:"+contains);
        /*
           boolean remove(Object obj)
           从集合中删除与给定元素equals比较为true的元素
           如果集合中存在多个这样的元素,仅会删除一个.
         */
        c.remove(p);
        System.out.println(c);
    }
}




