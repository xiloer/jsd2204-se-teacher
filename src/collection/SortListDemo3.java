package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 当集合元素已经实现了Comparable接口并定义了比较规则,但是这个规则不能满足我们的
 * 排序需求时,我们也可以临时指定一个规则进行排序
 * 例如:排序字符串
 */
public class SortListDemo3 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        //通过对英文的排序我们可以看出字符串是按照字符的unicode编码对应的正数比大小的
//        list.add("tom");
//        list.add("jerry");
//        list.add("jack");
//        list.add("rose");
//        list.add("Ada");
//        list.add("Kobe");
//        list.add("james");
//        list.add("jill");
//        list.add("hanmeimei");

        list.add("苍老师");
        list.add("传奇");
        list.add("小泽老师");
        System.out.println(list);
//        Collections.sort(list);
        //临时提供一个比较规则,按照字符多少排序
        Collections.sort(list,(s1,s2)->s1.length()-s2.length());
        //两个数反过来相减就是降序
//        Collections.sort(list,(s1,s2)->s2.length()-s1.length());
        System.out.println(list);
    }
}
