package collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * 集合转换为数组
 *
 */
public class CollectionToArrayDemo {
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        System.out.println(c);
        //返回对象数组的toArray现在用的非常少了，不方便。
//        Object[] array = c.toArray();
        /*
            重载的toArray方法要求我们传入一个数组(需要转换为什么类型的数组
            就应当传入一个什么类型的数组，但是需要注意该类型应当与当前集合的
            泛型一致,即元素类型一致)。
            传入的数组长度没有限制，toArray方法如果判断传入的数组可用就直接
            将集合元素存入该数组后将其返回(当数组长度>=集合的size)否则直接
            创建一个与集合size一样长的数组。
         */
        String[] array = c.toArray(new String[c.size()]);
//        String[] array = c.toArray(new String[10]);
//        String[] array = c.toArray(new String[1]);

        System.out.println(Arrays.toString(array));
    }
}







