package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * java.util.List
 * List表示的是可重复且有序的集合。也称为"线性表"
 * List常用的实现类:
 * java.util.ArrayList:内部使用数组实现，查询性能更好。
 * java.util.LinkedList:内部使用链表实现，增删性能更好，首尾增删元素最佳。
 * 在对性能没有特别苛刻的要求时，首选使用ArrayList
 */
public class ListDemo1 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        System.out.println(list);
        /*
            E get(int index)
            获取指定位置上的元素
         */
        String str = list.get(2);//array[2];
        System.out.println(str);

        for(int i=0;i<list.size();i++){
            str = list.get(i);
            System.out.println(str);
        }

        /*
            E set(int index,E e)
            将给定元素e设置到指定位置index处
            返回值就是被替换下来的元素
         */
        //[one,two,six,four,five]
        String old = list.set(2,"six");
        System.out.println(list);
        System.out.println(old);

        //集合反转
        //   v                v
        //[five,four,six,two,one]
        //  0    1    2   3   4
//        Collections.reverse(list);
        for(int i=0;i<list.size()/2;i++){
            //获取正数位置元素
            String s = list.get(i);
            //将该元素设置到倒数位置上并获取该倒数位置的元素
            s = list.set(list.size()-1-i,s);
            //再将倒数位置的元素设置到正数位置上
            list.set(i,s);
        }

        System.out.println(list);


    }
}







