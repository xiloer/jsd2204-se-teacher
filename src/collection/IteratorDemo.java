package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 集合的遍历
 *
 * Collection提供的方法:
 * Iterator iterator()        die
 * 该方法会返回一个可以遍历当前集合的迭代器
 *
 * java.util.Iterator迭代器接口
 * 不同的集合都提供了一个可以遍历自身元素的迭代器实现类.我们无需知道实现类的具体名字
 * 只要当他们是Iterator去使用即可.
 * 迭代器遍历集合遵循的步骤为:问->取->删
 * 其中删除操作不是遍历过程中的必须操作.
 */
public class IteratorDemo {
    public static void main(String[] args) {
        Collection c = new ArrayList();
        c.add("one");
        c.add("#");
        c.add("two");
        c.add("#");
        c.add("three");
        c.add("#");
        c.add("four");
        c.add("#");
        c.add("five");
        System.out.println(c);

        Iterator it = c.iterator();
        /*
            has:有
            next:下一个

            boolean hasNext()
            通过迭代器当前位置判断是否还有下一个元素可以遍历

            E next()
            迭代器向后移动一个元素的位置,并获取该元素.
            注:1:迭代器默认的起始位置是集合第一个元素之前的位置.
               2:在调用next()方法前必须确保hasNext()方法返回值为true
                 否则可能会抛出异常:
                 java.util.NoSuchElementException
                           没有这个元素异常

            迭代器在遍历的过程中不得通过集合的方法增删元素,否则遍历过程中会
            抛出异常:java.util.ConcurrentModificationException

         */
        while(it.hasNext()){
            String str = (String)it.next();
            if("#".equals(str)){
//                c.remove(str);
                it.remove();//删除迭代器当前位置表示的集合元素
            }
            System.out.println(str);
        }


        System.out.println(c);
    }
}












