package collection;

import java.util.ArrayList;
import java.util.List;

/**
 * List subList(int start,int end)
 * 获取当前集合中指定范围内的子集。两个参数为开始下标和结束下标的位置。含头不含尾
 */
public class ListDemo3 {
    public static void main(String[] args) {
        //泛型只能指定引用类型，对于基本类型而言要用对应的包装类
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<10;i++){
            list.add(i);
        }
        System.out.println(list);

        List<Integer> subList = list.subList(3,8);
        System.out.println(subList);

        //将子集每个元素扩大10倍
//        for(int i=0;i<subList.size();i++){
//            int num = subList.get(i);
//            num = num * 10;
//            subList.set(i,num);
//        }
        for(int i=0;i<subList.size();i++){
            subList.set(i,subList.get(i) * 10);
        }
        //[30,40,50,60,70]
        System.out.println(subList);
        /*
            对子集的操作就是对原集合对应的操作。
         */
        System.out.println(list);

        //删除当前list集合中2-8
        list.subList(2,9).clear();
        System.out.println(list);

    }
}
