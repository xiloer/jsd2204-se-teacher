package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 排序自定义类型元素的集合
 */
public class SortListDemo2 {
    public static void main(String[] args) {
        List<Point> list = new ArrayList<>();
        list.add(new Point(1,2));
        list.add(new Point(9,8));
        list.add(new Point(0,3));
        list.add(new Point(5,4));
        list.add(new Point(12,23));
        list.add(new Point(14,8));
        System.out.println(list);
        /*
            compare:比较
            comparable:可比较的

            Collections在排序一个List集合时,要求集合元素必须实现Comparable
            接口,否则编译不通过.
            该操作不推荐使用,因为对我们的代码有侵入性!
            侵入性:
            当我们使用某个API时,其要求我们为其修改其他额外地方的代码,就是侵入性
            它不利于程序后期的维护.
         */
//        Collections.sort(list);
//        Comparator<Point> c = new Comparator<Point>() {
//            /*
//                该方法重写的目的用于给出o1与o2谁大谁小.
//                返回值是一个int值,表达o1与o2的大小关系.
//                当返回值>0时:表示经过判断o1是大于o2的
//                当返回值<0时:表示o1<o2
//                当返回值=0时:表示o1与o2相等
//             */
//            public int compare(Point o1, Point o2) {
//                int olen1 = o1.getX()*o1.getX()+o1.getY()*o1.getY();
//                int olen2 = o2.getX()*o2.getX()+o2.getY()*o2.getY();
//                return olen1-olen2;
//            }
//        };
//        Collections.sort(list,c);

//        Collections.sort(list,new Comparator<Point>() {
//            public int compare(Point o1, Point o2) {
//                int olen1 = o1.getX()*o1.getX()+o1.getY()*o1.getY();
//                int olen2 = o2.getX()*o2.getX()+o2.getY()*o2.getY();
//                return olen1-olen2;
//            }
//        });

        Collections.sort(list,
                (o1,o2)->
                    o1.getX()*o1.getX()+o1.getY()*o1.getY()-
                    o2.getX()*o2.getX()-o2.getY()*o2.getY()
        );


        System.out.println(list);
    }
}
