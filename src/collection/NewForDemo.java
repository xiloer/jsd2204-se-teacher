package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * JDK5之后推出了一个新的特性:增强型for循环.
 * 也称为"新循环".语法:
 * for(元素类型 变量名 : 集合或数组){
 *     ...
 * }
 * 注意:新循环只用来遍历集合或数组,不取代传统for循环的工作.
 */
public class NewForDemo {
    public static void main(String[] args) {
        String[] array = {"one","two","three","four","five"};
        for(int i=0;i<array.length;i++){
            String str = array[i];
            System.out.println(str);
        }
        /*
            新循环语法是编译器认可,而非虚拟机.编译器会将新循环遍历数组改为普通的
            for循环遍历.
         */
        for(String str : array){
            System.out.println(str);
        }
        /*
            泛型,JDK5出现时出现的另一个特性
            泛型也称为参数化类型,允许我们在使用一个类时去指定该类中某个属性,方法
            参数,方法返回值的类型.这样可以大大的提高代码的灵活度.
            泛型在集合中被广泛使用,用来在使用一个集合是指定该集合的元素类型.
            如果不指定,默认为Object类型.
         */
        Collection<String> c = new ArrayList<>();
        c.add("一");
        c.add("二");
        c.add("三");
        c.add("四");
        c.add("五");


        Iterator<String> it = c.iterator();
        while(it.hasNext()){
            String o = it.next();
            System.out.println(o);
        }
        /*
            新循环遍历集合会改成迭代器遍历.
         */
        for(String o : c){
            System.out.println(o);
        }


    }
}





