package reflect;

import java.lang.reflect.Constructor;

/**
 * 使用指定的有参构造器实例化对象
 */
public class ReflectDemo3 {
    public static void main(String[] args) throws Exception {
        Person p = new Person("李四",55);
        System.out.println(p);

        Class cls = Class.forName("reflect.Person");
//        Constructor c = cls.getConstructor();//获取无参构造器
        //Person(String,int)
        Constructor c = cls.getConstructor(String.class,int.class);
        Object obj = c.newInstance("王五",66);//new Person("王五",66);
        System.out.println(obj);
    }
}
