package reflect;

import java.lang.reflect.Method;

/**
 * 反射机制访问私有成员
 * 需要注意,这可能会破坏类原有的封装性,使用时要保持谨慎的态度.
 */
public class ReflectDemo6 {
    public static void main(String[] args) throws Exception {
//        Person p = new Person();
//        p.heihei();//编译不通过,类的外部不可以调用一个类的私有成员

        Class cls = Class.forName("reflect.Person");
        Object obj = cls.newInstance();
        //getMethod只能获取公开方法
//        Method method = cls.getMethod("heihei");

        //获取私有方法可以用getDeclaredMethod
        Method method = cls.getDeclaredMethod("heihei");
        method.setAccessible(true);//强行打开访问权限
        method.invoke(obj);
    }
}
