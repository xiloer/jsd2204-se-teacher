package reflect;

import java.lang.reflect.Method;

/**
 * 调用有参数方法
 */
public class ReflectDemo5 {
    public static void main(String[] args) throws Exception {
        Person p = new Person();
        p.say("你好");
        p.say("hehe",5);

        Class cls = Class.forName("reflect.Person");
        Object obj = cls.newInstance();
        //say(String)
        Method method = cls.getMethod("say",String.class);
        method.invoke(obj,"大家好");

        //say(String,int)
        Method method1 = cls.getMethod("say", String.class, int.class);
        method1.invoke(obj,"呵呵",5);
    }
}




