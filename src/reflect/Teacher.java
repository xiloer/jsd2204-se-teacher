package reflect;

import reflect.annotations.AutoRunClass;
import reflect.annotations.AutoRunMethod;

@AutoRunClass
public class Teacher {
    @AutoRunMethod(10)
    public void teach(){
        System.out.println("老师:正在上课");
    }

    public void eat(){
        System.out.println("老师:正在干饭!");
    }
}
