package reflect;

import reflect.annotations.AutoRunClass;
import reflect.annotations.AutoRunMethod;

import java.io.File;
import java.lang.reflect.Method;

/**
 * 自动扫描并实例化与当前类Test3在同一个包中那些被@AutoRunClass标注的类，
 * 并自动调用其中被注解@AutoRunMethod标注的方法
 */
public class Test3 {
    public static void main(String[] args) throws Exception {
        File dir = new File(Test3.class.getResource(".").toURI());
        File[] files = dir.listFiles(f->f.getName().endsWith(".class"));
        for(File file : files) {
            String fileName = file.getName();//Test3.class
            //根据文件名确定类名
            String className = fileName.substring(0, fileName.indexOf("."));//Test2
            Class cls = Class.forName(
                    Test3.class.getPackage().getName() + "." + className
            );
            if(cls.isAnnotationPresent(AutoRunClass.class)) {
                Object obj = cls.newInstance();
                System.out.println(obj);

                Method[] methods = cls.getDeclaredMethods();
                for(Method method : methods){
                    if(method.isAnnotationPresent(AutoRunMethod.class)){
                        //通过注解@AutoRunMethod的参数来确定自动执行该方法的次数
                        AutoRunMethod arm = method.getAnnotation(AutoRunMethod.class);
                        int value = arm.value();//获取注解@AutoRunMethod上的参数value

                        System.out.println("自动调用方法:"+method.getName()+"()"+value+"次");
                        for(int i=0;i<value;i++){
                            method.invoke(obj);
                        }
                    }
                }

            }
        }

    }
}
