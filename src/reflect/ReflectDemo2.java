package reflect;

import java.util.Scanner;

/**
 * 利用反射机制实例化对象
 */
public class ReflectDemo2 {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Person p = new Person();
        System.out.println(p);
        /*
            反射机制实例化
            1:加载要实例化对象的类的类对象
            2:调用Class的方法newInstance()利用无参构造器实例化
         */
//        Class cls = Class.forName("reflect.Person");
         /*
            reflect.Person
            java.lang.String
            java.util.HashMap
            java.util.Date
         */
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一个类名:");
        String className = scanner.nextLine();
        Class cls = Class.forName(className);

        Object obj = cls.newInstance();//new Person()
        System.out.println(obj);
    }
}





