package reflect;

import reflect.annotations.AutoRunClass;

/**
 * 使用反射机制访问注解
 */
public class ReflectDemo7 {
    public static void main(String[] args) throws Exception {
//        Class cls = Class.forName("reflect.Person");
        Class cls = Class.forName("reflect.ReflectDemo7");
        //查看Person是否被注解@AutoRunClass标注过?
        boolean is = cls.isAnnotationPresent(AutoRunClass.class);
        System.out.println("是否被标注了?"+is);

    }
}
