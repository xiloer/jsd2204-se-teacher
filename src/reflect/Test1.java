package reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * 自动调用Person类中所有本类定义的公开的方法名含字母"a"的无参数方法.
 * 思路:
 * 1:获取Person本类定义的所有方法
 * 2:如果该方法的参数个数为0则使用invoke执行该方法
 *   注:Method提供了一个方法:
 *   int getParameterCount()
 *   方法返回的int值表示该方法的参数个数.
 */
public class Test1 {
    public static void main(String[] args) throws Exception {
        Class cls = Class.forName("reflect.Person");
        Object obj = cls.newInstance();
        //获取本类定义的所有方法
        Method[] methods = cls.getDeclaredMethods();
        for(Method method : methods){
            if(method.getParameterCount()==0  //是否为无参方法
                    &&
                method.getModifiers() == Modifier.PUBLIC
                    &&
                method.getName().contains("a")
            ){
//                method.setAccessible(true);
                System.out.println("自动调用方法:"+method.getName()+"()");
                method.invoke(obj);
            }
        }


    }
}
