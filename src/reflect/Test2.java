package reflect;

import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * 自动调用那些与当前类Test2在同一个包中那些类中的公开的,无参的方法
 */
public class Test2 {
    public static void main(String[] args) throws Exception{
        /*
            开发中常用的相对路径:
            类名.class.getResource(".")
            对应的就是该类所在的目录(字节码文件所在的目录)

            package reflect.abc;
            Test2
         */
        File dir = new File(Test2.class.getResource(".").toURI());
        File[] files = dir.listFiles(f->f.getName().endsWith(".class"));
        System.out.println(files.length);
        for(File file : files){
            String fileName = file.getName();//Test2.class
            //根据文件名确定类名
            String className = fileName.substring(0,fileName.indexOf("."));//Test2
            Class cls = Class.forName(Test2.class.getPackage().getName()+"."+className);
            Object obj = cls.newInstance();
            //获取本类定义的所有方法
            Method[] methods = cls.getDeclaredMethods();
            for(Method method : methods){

                if(method.getParameterCount()==0  //是否为无参方法
                        &&
                        method.getModifiers() == Modifier.PUBLIC
                ){
                    System.out.println("自动调用方法:"+method.getName()+"()");
                    method.invoke(obj);
                }
            }
        }
    }
}
