package reflect.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注解
 * 注解被大量应用于反射机制中，可以辅助反射机制做很多灵活的操作。
 * java行业中流行的框架基本都是靠注解来进行相关的配置了。
 *
 * 我们在定义一个注解时，通常会使用两个java定义的专门说明注解操作的注解。
 * @Target  该注解用于说明当前注解的可标记位置。使用ElementType规定的范围。
 *
 * @Retention 该注解用于说明当前注解的保留级别，有三个可选值
 *            RetentionPolicy.SOURCE 注解仅保留在源代码中，编译后的字节码文件中不存在
 *            RetentionPolicy.CLASS  保留在字节码文件中，但是反射机制访问不到。默认保留级别。
 *            RetentionPolicy.RUNTIME 保留在字节码文件中，反射机制可以访问到该注解。
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AutoRunClass {
}
