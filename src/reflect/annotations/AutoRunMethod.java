package reflect.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AutoRunMethod {
    /**
     * 可以为注解定义参数
     * 格式:参数类型 参数名() [default 默认值]   注:[]的内容可不写
     * 如果该注解只有一个参数时，参数名应当使用value.
     *
     * 当注解只有一个参数且名字为value时，使用该注解传参可忽略参数名，写法如下:
     * @AutoRunMethod(5)
     * 此时这里的我5就是value的值
     * @AutoRunMethod
     * 此时没有传参时，value用默认值，前提时value使用default指定了默认值。
     *
     * 如果注解只有一个参数，但是名字不叫value，比如
     * public @interface AutoRunMethod {
     *      int num default 1;
     * }
     * 那么使用该注解时，传参格式必须为:参数名=参数值，写法如下:
     * @AutoRunMethod(num=1)
     * 或
     * @AutoRunMethod  注:使用默认值
     *
     * 如果注解有多个参数时，参数名则不必叫value，就算叫value也不会出现上述情况
     * 例如:
     * public @interface AutoRunMethod {
     *      int num default 1;
     *      String name default "mike";
     * }
     * 那么使用该注解时，写法如下:
     * @AutoRunMethod   注:num为1  name为"mike" 即:默认值
     * @AutoRunMethod(num=2)  注:num为2  name为"mike"  即:name用默认值
     *
     * @AutoRunMethod(num=3,name="jack")
     * 或
     * @AutoRunMethod(name="jack",num=3)
     * 两个以上参数时，参数的顺序没有讲究。
     */
    int value() default 1;
//    String name() default "abc";
}
