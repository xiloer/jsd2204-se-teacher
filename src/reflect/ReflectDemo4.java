package reflect;

import java.lang.reflect.Method;
import java.util.Scanner;

/**
 * 使用反射机制调用方法
 */
public class ReflectDemo4 {
    public static void main(String[] args) throws Exception {
        Person p = new Person();
        p.sayHello();


        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入类名:");
        String className = scanner.nextLine();
        System.out.println("请输入方法名:");
        String methodName = scanner.nextLine();

        //1实例化
//        Class cls = Class.forName("reflect.Person");
        Class cls = Class.forName(className);
        Object obj = cls.newInstance();

        //2调用sayHello方法
        //2.1通过类对象获取要调用的方法
//        Method method = cls.getMethod("sayHello");//获取无参的sayHello方法
        Method method = cls.getMethod(methodName);//获取无参的sayHello方法
        //2.2通过方法对象来执行该方法
        method.invoke(obj);//p.sayHello();
    }
}
