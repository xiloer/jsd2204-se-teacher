package reflect;

import java.util.Arrays;

/**
 * JDK5之后,推出了一个特性:可变长参数
 * 用于那些方法参数个数不固定的情况下.
 */
public class ArgDemo {
    public static void main(String[] args) {
//        dosome(1,2);
//        dosome(1,2,"one");
//        dosome(1,2,"one","two");
//        dosome(1,2,"one","one","one","one","one","one","one","one","one","one","one","one");

//        a("1","2");
    }
    public static void a(String... a){
        System.out.println("b");
    }
    public static void a(String a1,String a2){
        System.out.println("a");
    }

    /**
     * 变长参数只能是一个方法中最后一个参数!
     * 变长参数实际上是一个数组.
     */
    public static void dosome(int a,long b,String... s){
        System.out.println(s.length);
        System.out.println(Arrays.toString(s));
    }
}
